Name:      perl-Config-Perl-V
Version:   0.36
Release:   2
Summary:   A module that will return you the output of 'perl -V' in a structure
License:   GPL-1.0-or-later OR Artistic-1.0-Perl
URL:       https://metacpan.org/release/Config-Perl-V
Source0:   https://cpan.metacpan.org/authors/id/H/HM/HMBRAND/Config-Perl-V-%{version}.tgz

BuildArch:      noarch
BuildRequires:  coreutils make perl-generators perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(Test::More) perl(Test::NoWarnings)
Suggests:       perl(Digest::MD5)

%description
The command 'perl -V' will return you an excerpt from the %Config hash
combined with the output of 'perl -V' that is not stored inside %Config,
but only available to the perl binary itself.

%package_help

%prep
%autosetup -n Config-Perl-V-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%doc Changelog README
%{perl_vendorlib}/*

%files help
%doc examples/ CONTRIBUTING.md
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 0.36-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jul 12 2023 leeffo <liweiganga@uniontech.com> - 0.36-1
- upgrade version to 0.36

* Tue Oct 25 2022 renhongxun <renhongxun@h-partners.com> - 0.33-2
- Rebuild for next release

* Wed Jan 27 2021 liudabo <liudabo1@huawei.com> - 0.33-1
- upgrade version to 0.33

* Thu Jul 23 2020 shixuantong<shixuantong@huawei.com> - 0.31-1
- Type: NA
- ID: NA
- SUG: NA
- DESC:update to 0.31-1

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.30-5
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: remove unnecessary files

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.30-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: change the directory of README and examples/ 

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.30-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove some notes

* Fri Aug 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.30-2
- Package init
